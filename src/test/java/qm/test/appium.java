package qm.test;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class appium {
	
	public AndroidDriver<WebElement> driver;

	@Before
	public void setUp() throws Exception {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Android device");
		caps.setCapability("appPackage", "com.quickmobile.app293");
		caps.setCapability("appActivity", "com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(3000);
		driver.closeApp();
	}

	@Test
	public void test1() throws InterruptedException {
		Thread.sleep(3000);
		System.out.println("The tese case 1 is Pass");
		System.out.println("master");
	}
	
	@Test
	public void test2() throws InterruptedException {
		Thread.sleep(3000);
		System.out.println("The tese case 2 is Pass");
	}
	
	@Test
	public void test3() throws InterruptedException {
		Thread.sleep(3000);
		System.out.println("The tese case 3 is Pass");
	}

}
